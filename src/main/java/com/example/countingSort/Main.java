package com.example.countingSort;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        int[] array = CountingSort.createRandomArray(20, -30, 100);
        System.out.println("Array before sorting:");
        System.out.println(Arrays.toString(array));
        System.out.println("Sorted array:");
        CountingSort.countingSort(array);
        System.out.println(Arrays.toString(array));
    }
}