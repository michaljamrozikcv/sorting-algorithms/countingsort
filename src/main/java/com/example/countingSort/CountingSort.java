package com.example.countingSort;

import java.util.Arrays;
import java.util.Random;

public class CountingSort {
    /**
     * złożoność obliczeniowa: O(n+k), gdzie n-liczba elementów sortowanej tablicy, k-liczba elementów tablicy zliczającej
     * złożoność pamięciowa: Omega(k),
     * Method works also on negative numbers
     */
    static void countingSort(int[] array) {
        //find the lowest and highest element in array
        int min = Arrays.stream(array).min().getAsInt();
        int max = Arrays.stream(array).max().getAsInt();
        /*establish size of counting array; indexes of counting array are all integer numbers from range [min, max]
        moved by value of min*/
        int size = max - min + 1;
        /*counting array - elements of this array are counters of existence values in original array
        eg. counter on index 0 means how many times number 0 + min appeared in original array*/
        int[] countingArray = new int[size];
        for (int i = 0; i < array.length; i++) {
            countingArray[array[i] - min]++;
        }
        //original array overriding
        int index = 0;
        for (int i = 0; i < size; i++) {
            int tempCounter = countingArray[i];
            while (tempCounter > 0) {
                array[index] = i + min;
                index++;
                tempCounter--;
            }
        }
    }

    /**
     * Return array of random integers
     *
     * @param size   the number of values to generate
     * @param origin the origin (inclusive) of each random value
     * @param bound  the bound (exclusive) of each random value
     */
    static int[] createRandomArray(int size, int origin, int bound) {
        int[] array = new int[size];
        Random random = new Random();
        for (int i = 0; i < size; i++) {
            array[i] = random.ints(1, origin, bound).findFirst().getAsInt();
        }
        return array;
    }
}
